<?php 

    function acf_load_menu_field_choices( $field ) {
        $wp_menus = wp_get_nav_menus();
        $choices = [];
        foreach ($wp_menus as $menu) {
            $choices[$menu->slug] = $menu->name; 
        }
        $field['choices'] = $choices;
        return $field;
    }
    add_filter('acf/load_field/key=field_61d8671f98e40', 'acf_load_menu_field_choices');

?>
