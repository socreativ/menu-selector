<?php

$id = isset($block['anchor']) && $block['anchor'] ? $block['anchor'] : $block['id'];
$css = isset($block['className']) ? $block['className'] : '';

?>

<section id="<?= $id ?>" class="menu-selector <?= $css ?>" >

    <?php wp_nav_menu(array('slug' => get_field('menu'))); ?>

    <div class="menu-follower"></div>

</section>